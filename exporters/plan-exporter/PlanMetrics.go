package main

const (
	FAILURE = 0
	SUCCESS = 1
	PENDING = 2
)

type PlanMetrics struct {
	plan_export_last_status         int
	plan_export_total               int
	plan_export_success_total       int
	plan_export_failure_total       int
	plan_export_daily_status        map[string]int
	plan_export_daily_total         map[string]int
	plan_export_success_daily_total map[string]int
	plan_export_failure_daily_total map[string]int
}

func NewPlanMetrics() *PlanMetrics {
	return &PlanMetrics{
		plan_export_last_status:         PENDING,
		plan_export_total:               0,
		plan_export_success_total:       0,
		plan_export_failure_total:       0,
		plan_export_daily_status:        make(map[string]int),
		plan_export_daily_total:         make(map[string]int),
		plan_export_success_daily_total: make(map[string]int),
		plan_export_failure_daily_total: make(map[string]int),
	}
}

func (metrics *PlanMetrics) Success(date string) {
	metrics.plan_export_last_status = SUCCESS
	metrics.plan_export_total++
	metrics.plan_export_success_total++

	metrics.plan_export_daily_status[date] = SUCCESS
	AddOrIncrement(metrics.plan_export_daily_total, date)
	AddOrIncrement(metrics.plan_export_success_daily_total, date)
}

func (metrics *PlanMetrics) Failure(date string) {
	metrics.plan_export_last_status = FAILURE
	metrics.plan_export_total++
	metrics.plan_export_failure_total++

	metrics.plan_export_daily_status[date] = FAILURE
	AddOrIncrement(metrics.plan_export_daily_total, date)
	AddOrIncrement(metrics.plan_export_failure_daily_total, date)
}
