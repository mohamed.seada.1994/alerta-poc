package main

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

// Metrics:
// 01- plan_export_status{"plan":"plan-id"} gauge => 0,1
// 02- plan_export_last_status{"plan":"plan-id"} gauge => 0,1
// 03- plan_export_total{"plan":"plan-id"} counter
// 04- plan_export_success_total{"plan":"plan-id"} counter
// 05- plan_export_failure_total{"plan":"plan-id"} counter
// ---
// 06- plan_export_today_total{"plan":"plan-id"} counter
// 07- plan_export_success_today_total{"plan":"plan-id"} counter
// 08- plan_export_failure_today_total{"plan":"plan-id"} counter
// ---
// 09- plan_export_daily_status{"plan":"plan-id", "date":"20220613"} gauge => 0,1 the final status in the end of a day
// 10- plan_export_daily_total{"plan":"plan-id", "date":"20220613"} counter
// 11- plan_export_success_daily_total{"plan":"plan-id", "date":"20220613"} counter
// 12- plan_export_failure_daily_total{"plan":"plan-id", "date":"20220613"} counter

type PlanCollector struct {
	manager *PlanManager

	plan_export_status        *prometheus.Desc
	plan_export_last_status   *prometheus.Desc
	plan_export_total         *prometheus.Desc
	plan_export_success_total *prometheus.Desc
	plan_export_failure_total *prometheus.Desc

	plan_export_today_total         *prometheus.Desc
	plan_export_success_today_total *prometheus.Desc
	plan_export_failure_today_total *prometheus.Desc

	plan_export_daily_status        *prometheus.Desc
	plan_export_daily_total         *prometheus.Desc
	plan_export_success_daily_total *prometheus.Desc
	plan_export_failure_daily_total *prometheus.Desc
}

func NewPlanCollector(name, logFile string, expectedYesterday bool) *PlanCollector {
	return &PlanCollector{
		manager: NewPlanManager(name, logFile, expectedYesterday),
		plan_export_status: prometheus.NewDesc("plan_export_status",
			"The current status of the plan export tasks",
			nil, // variable labels
			prometheus.Labels{
				"plan": name,
			},
		),

		plan_export_last_status: prometheus.NewDesc("plan_export_last_status",
			"The status of the last plan export task",
			nil, // variable labels
			prometheus.Labels{
				"plan": name,
			},
		),
		plan_export_total: prometheus.NewDesc("plan_export_total",
			"The total count of the plan export tasks",
			nil, // variable labels
			prometheus.Labels{
				"plan": name,
			},
		),
		plan_export_success_total: prometheus.NewDesc("plan_export_success_total",
			"The total count of the succeed plan export tasks",
			nil, // variable labels
			prometheus.Labels{
				"plan": name,
			},
		),
		plan_export_failure_total: prometheus.NewDesc("plan_export_failure_total",
			"The total count of the failed plan export tasks",
			nil, // variable labels
			prometheus.Labels{
				"plan": name,
			},
		),

		plan_export_today_total: prometheus.NewDesc("plan_export_today_total",
			"The total count of the plan export tasks today",
			nil, // variable labels
			prometheus.Labels{
				"plan": name,
			},
		),
		plan_export_success_today_total: prometheus.NewDesc("plan_export_success_today_total",
			"The total count of the succeed plan export tasks today",
			nil, // variable labels
			prometheus.Labels{
				"plan": name,
			},
		),
		plan_export_failure_today_total: prometheus.NewDesc("plan_export_failure_today_total",
			"The total count of the failed plan export tasks today",
			nil, // variable labels
			prometheus.Labels{
				"plan": name,
			},
		),

		plan_export_daily_status: prometheus.NewDesc("plan_export_daily_status",
			"The status of the plan export tasks on specific day",
			[]string{"date"},
			prometheus.Labels{
				"plan": name,
			},
		),
		plan_export_daily_total: prometheus.NewDesc("plan_export_daily_total",
			"The total count of the plan export tasks on specific day",
			[]string{"date"},
			prometheus.Labels{
				"plan": name,
			},
		),
		plan_export_success_daily_total: prometheus.NewDesc("plan_export_success_daily_total",
			"The total count of the succeed plan export tasks on specific day",
			[]string{"date"},
			prometheus.Labels{
				"plan": name,
			},
		),
		plan_export_failure_daily_total: prometheus.NewDesc("plan_export_failure_daily_total",
			"The total count of the failed plan export tasks on specific day",
			[]string{"date"},
			prometheus.Labels{
				"plan": name,
			},
		),
	}
}

func (collector *PlanCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- collector.plan_export_last_status
	ch <- collector.plan_export_total
	ch <- collector.plan_export_success_total
	ch <- collector.plan_export_failure_total

	ch <- collector.plan_export_status
	ch <- collector.plan_export_today_total
	ch <- collector.plan_export_success_today_total
	ch <- collector.plan_export_failure_today_total

	ch <- collector.plan_export_daily_status
	ch <- collector.plan_export_daily_total
	ch <- collector.plan_export_success_daily_total
	ch <- collector.plan_export_failure_daily_total
}

func (collector *PlanCollector) Collect(ch chan<- prometheus.Metric) {

	ch <- prometheus.MustNewConstMetric(collector.plan_export_status, prometheus.GaugeValue, float64(collector.manager.GetPlanStatus()))
	ch <- prometheus.MustNewConstMetric(collector.plan_export_last_status, prometheus.GaugeValue, float64(collector.manager.metrics.plan_export_last_status))

	ch <- prometheus.MustNewConstMetric(collector.plan_export_total, prometheus.CounterValue, float64(collector.manager.metrics.plan_export_total))
	ch <- prometheus.MustNewConstMetric(collector.plan_export_success_total, prometheus.CounterValue, float64(collector.manager.metrics.plan_export_success_total))
	ch <- prometheus.MustNewConstMetric(collector.plan_export_failure_total, prometheus.CounterValue, float64(collector.manager.metrics.plan_export_failure_total))

	today := time.Now().Format("20060102")
	ch <- prometheus.MustNewConstMetric(collector.plan_export_today_total, prometheus.CounterValue, float64(TryGet(collector.manager.metrics.plan_export_daily_total, today, 0)))
	ch <- prometheus.MustNewConstMetric(collector.plan_export_success_today_total, prometheus.CounterValue, float64(TryGet(collector.manager.metrics.plan_export_success_daily_total, today, 0)))
	ch <- prometheus.MustNewConstMetric(collector.plan_export_failure_today_total, prometheus.CounterValue, float64(TryGet(collector.manager.metrics.plan_export_failure_daily_total, today, 0)))

	for date, status := range collector.manager.metrics.plan_export_daily_status {
		ch <- prometheus.MustNewConstMetric(collector.plan_export_daily_status, prometheus.GaugeValue, float64(status), date)
	}
	for date, count := range collector.manager.metrics.plan_export_daily_total {
		ch <- prometheus.MustNewConstMetric(collector.plan_export_daily_total, prometheus.CounterValue, float64(count), date)
	}
	for date, count := range collector.manager.metrics.plan_export_success_daily_total {
		ch <- prometheus.MustNewConstMetric(collector.plan_export_success_daily_total, prometheus.CounterValue, float64(count), date)
	}
	for date, count := range collector.manager.metrics.plan_export_failure_daily_total {
		ch <- prometheus.MustNewConstMetric(collector.plan_export_failure_daily_total, prometheus.CounterValue, float64(count), date)
	}
}
