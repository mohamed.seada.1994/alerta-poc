package main

import (
	"io/ioutil"
	"log"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Host  *HostConfig  `yaml:"host"`
	Plans []PlanConfig `yaml:"plans"`
}

type HostConfig struct {
	Port            string `yaml:"port"`
	MetricsEndpoint string `yaml:"metricsEndpoint"`
}

type PlanConfig struct {
	Name              string `yaml:"name"`
	LogFile           string `yaml:"logFile"`
	ExpectedYesterday bool   `yaml:"expectedYesterday"`
}

func ReadConfig(filePath string) *Config {

	yamlFile, err := ioutil.ReadFile(filePath)
	if err != nil {
		log.Println("Error reading config file")
		log.Panicln(err)
	}

	var config = &Config{}
	err = yaml.Unmarshal(yamlFile, config)
	if err != nil {
		log.Println("Error parsing config file")
		log.Panicln(err)
	}

	return config
}
