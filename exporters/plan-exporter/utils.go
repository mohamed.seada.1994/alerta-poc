package main

func AddOrIncrement(data map[string]int, key string) {
	_, exists := data[key]
	if exists {
		data[key]++
	} else {
		data[key] = 1
	}
}

func TryGet(data map[string]int, key string, defaultValue int) int {
	_, exists := data[key]
	if exists {
		return data[key]
	} else {
		return defaultValue
	}
}
