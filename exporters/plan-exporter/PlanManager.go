package main

import (
	"context"
	"io"
	"strings"
	"time"

	"github.com/enriquebris/goconcurrentqueue"
	"github.com/go-faster/tail"
)

const LOG_EXPIRE_AFTER_DAYS = 31

type PlanManager struct {
	planName          string
	logFile           string
	expectedYesterday bool
	metrics           *PlanMetrics
	_entriesQueue     *goconcurrentqueue.FIFO
}

func NewPlanManager(name, logFile string, expectedYesterday bool) *PlanManager {
	manager := PlanManager{
		planName:          name,
		logFile:           logFile,
		expectedYesterday: expectedYesterday,
		metrics:           NewPlanMetrics(),
		_entriesQueue:     goconcurrentqueue.NewFIFO(),
	}

	go manager.StartLogsReadLoop()
	go manager.StartMetricsUpdateLoop()

	return &manager
}

func (manager *PlanManager) StartLogsReadLoop() {
	logTail := tail.File(manager.logFile, tail.Config{
		Follow:     true,       // tail -f
		BufferSize: 1024 * 128, // 128 kb for internal reader buffer

		// Force polling if zero events are observed for longer than a minute.
		// Optional, just a safeguard to be sure that we are not stuck forever
		// if we miss inotify event.
		NotifyTimeout: 5 * time.Second, // TODO: read from config file

		Location: &tail.Location{Whence: io.SeekStart, Offset: 0},
	})

	var nextEntry *PlanLogEntry = nil
	ctx := context.Background()
	if err := logTail.Tail(ctx, func(ctx context.Context, line *tail.Line) error {
		text := strings.TrimSpace(string(line.Data))
		if len(text) == 0 {
			return nil
		}

		date, err := time.Parse("20060102", text)
		isDate := err == nil
		if !isDate && nextEntry != nil {
			nextEntry.Append(text)
		}

		if !isDate {
			return nil
		}

		if nextEntry != nil {
			nextEntry.Commit()
		}

		isObsoleteDate := date.Before(time.Now().AddDate(0, 0, -LOG_EXPIRE_AFTER_DAYS))
		if isObsoleteDate {
			nextEntry = nil
		} else {
			nextEntry = NewPlanLogEntry(text)
			manager._entriesQueue.Enqueue(nextEntry)
		}
		return nil
	}); err != nil {
		panic(err)
	}
}

func (manager *PlanManager) StartMetricsUpdateLoop() {
	for {
		entry := manager.WaitForNextEntry()
		entry.WaitCommit()

		if entry.IsSuccess() {
			manager.metrics.Success(entry.date)
		} else {
			manager.metrics.Failure(entry.date)
		}
	}
}

func (manager *PlanManager) WaitForNextEntry() *PlanLogEntry {
	entry, _ := manager._entriesQueue.DequeueOrWaitForNextElement()
	return entry.(*PlanLogEntry)
}

func (manager *PlanManager) GetPlanStatus() int {
	today := time.Now().Format("20060102")
	today_success_total := TryGet(manager.metrics.plan_export_success_daily_total, today, 0)

	if today_success_total > 0 {
		return SUCCESS
	}

	if !manager.expectedYesterday {
		return FAILURE
	}

	yesterday := time.Now().AddDate(0, 0, -1).Format("20060102")
	yesterday_success_total := TryGet(manager.metrics.plan_export_success_daily_total, yesterday, 0)

	if today_success_total > 0 || yesterday_success_total > 0 {
		return SUCCESS
	}

	return FAILURE
}
