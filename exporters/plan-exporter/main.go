package main

import (
	"fmt"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	var config = ReadConfig("./config/plan-exporter.yml")

	for _, plan := range config.Plans {
		prometheus.Register(NewPlanCollector(plan.Name, plan.LogFile, plan.ExpectedYesterday))
	}

	println("Exporter listening on port " + config.Host.Port)
	fmt.Printf("open http://localhost:%s%s\n", config.Host.Port, config.Host.MetricsEndpoint)

	http.Handle(config.Host.MetricsEndpoint, promhttp.Handler())
	http.ListenAndServe(":"+config.Host.Port, nil)
}
