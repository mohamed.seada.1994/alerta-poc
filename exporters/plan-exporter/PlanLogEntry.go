package main

import (
	"sync"
	"time"
)

const (
	COMMIT_DELAY  = 5 * time.Second
	SUCCESS_LINES = 4
)

type PlanLogEntry struct {
	date         string
	lines        int
	_commitMutex sync.Mutex
}

func NewPlanLogEntry(date string) *PlanLogEntry {
	entry := PlanLogEntry{date: date, lines: 1}
	entry._commitMutex.Lock()
	return &entry
}

func (entry *PlanLogEntry) Append(line string) {
	entry.lines++
	if entry.lines == SUCCESS_LINES {
		go func() {
			time.Sleep(COMMIT_DELAY)
			entry.Commit()
		}()
	}
}

func (entry *PlanLogEntry) Commit() {
	entry._commitMutex.Unlock()
}

func (entry *PlanLogEntry) WaitCommit() {
	entry._commitMutex.Lock()
}

func (entry *PlanLogEntry) IsSuccess() bool {
	return entry.lines == SUCCESS_LINES
}
