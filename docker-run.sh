docker network create promnet

# prometheus
docker run -v /config:/prometheus -v /data/prometheus:/data --restart=always --name alerta-prometheus --network=promnet -d -p 9090:9090 prom/prometheus:v2.36.2 /bin/prometheus --config.file=/prometheus/prometheus.yml --storage.tsdb.path=/data
docker run -v /config:/prometheus -v /data/alertmanager:/data --restart=always --name alerta-alertmanager --network=promnet -d -p 9093:9093 prom/alertmanager:v0.24.0 /bin/alertmanager --config.file=/prometheus/alertmanager.yml --storage.path=/data

# plan-exporter:1.0.0
docker build -t plan-exporter:1.0.0 ./exporters/plan-exporter
docker run -v /config:/config -v /logs:/logs --restart=always --name alerta-plan-exporter --network=promnet -d -p 2112:2112 plan-exporter:1.0.0

# alerta
docker build -t orange-alerta:8.6.3 ./alerta
docker run --restart=always --name alerta --network=promnet --network=db -d -p 9080:8080 orange-alerta:8.6.3
docker run -v /data/pg-data:/var/lib/postgresql/data --restart=always --name alerta-db --network=db -d -e POSTGRES_DB=monitoring -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres 14.4-bullseye
